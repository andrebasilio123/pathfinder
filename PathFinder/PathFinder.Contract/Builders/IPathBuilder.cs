﻿using System.Threading.Tasks;

namespace PathFinder.Contract.Builders
{
    public interface IPathBuilder
    {
        Task BuildAsync(string[] args);
    }
}
