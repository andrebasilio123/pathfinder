﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PathFinder.Contract.Readers
{
    public interface IReaderFile
    {
        Task<IEnumerable<string>> ReadAsync(string filePath);
    }
}
