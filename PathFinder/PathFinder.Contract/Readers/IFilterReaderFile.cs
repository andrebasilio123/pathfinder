﻿namespace PathFinder.Contract.Readers
{
    public interface IFilterReaderFile : IReaderFile
    {
        public IFilterWord GetFilter();
    }
}
