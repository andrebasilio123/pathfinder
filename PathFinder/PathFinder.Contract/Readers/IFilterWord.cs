﻿using System.Threading.Tasks;

namespace PathFinder.Contract.Readers
{
    public interface IFilterWord
    {
        Task<bool> FilterAsync(string word);
    }
}
