﻿namespace PathFinder.Contract.Exceptions
{
    public static class ErrorMessage
    {
        public readonly static string InvalidNumberOfArguments = "Invalid number of arguments : should be (startword endword sourcepath destination)";
        public readonly static string InvalidLengthOfWordsArguments = "Invalid length of words on the arguments";
    }
}
