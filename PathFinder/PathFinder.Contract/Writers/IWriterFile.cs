﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PathFinder.Contract.Writers
{
    public interface IWriterFile
    {
        Task WriteAsync(string filePath, IEnumerable<string> lines);
    }
}
