﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PathFinder.Contract.Finders
{
    public interface IFinderStrategy
    {
        Task<IEnumerable<string>> FindAsync(string startWord, string endWord, IEnumerable<string> lines);
    }
}
