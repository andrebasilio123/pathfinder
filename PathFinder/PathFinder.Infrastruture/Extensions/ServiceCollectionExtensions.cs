﻿using Microsoft.Extensions.DependencyInjection;
using PathFinder.Contract.Builders;
using PathFinder.Contract.Finders;
using PathFinder.Contract.Readers;
using PathFinder.Contract.Writers;
using PathFinder.Implementation.Builders;
using PathFinder.Implementation.Finders.QueueFinder;
using PathFinder.Implementation.Readers;
using PathFinder.Implementation.Writers;

namespace PathFinder.Infrastruture.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPathFinderContracts(this IServiceCollection services)
        {
            //Add PathFinder dependencies
            services.AddTransient<IPathBuilder, WordsFilteredPathBuilder>()
                    .AddTransient<IFilterWord, FourLettersFilterWord>()
                    .AddTransient<IFilterReaderFile, FilterReaderFile>()
                    .AddTransient<IWriterFile, TxtWriterFile>()
                    .AddTransient<IWriterFile, CsvWriterFile>()
                    .AddTransient<IFinderStrategy, QueueFinderStrategy>();

            return services;
        }
    }
}
