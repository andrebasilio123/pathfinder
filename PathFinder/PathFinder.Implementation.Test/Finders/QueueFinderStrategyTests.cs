﻿using PathFinder.Implementation.Finders.QueueFinder;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PathFinder.Implementation.Test.Finders
{
    public class QueueFinderStrategyTests
    {
        List<string> listTest;

        public QueueFinderStrategyTests()
        {
            listTest = new List<string>
            {
                "ADEU", "ODEU", "EDEU", "OLAF" , "OLEF", "ODEF", "AFIS", "ADES", "ADEF", 
                "ADIF", "AFIF", "ADIS", "KILO"
            };
        }

        [Theory]
        [InlineData("ADEU", "OLEF", "ADEU", "ODEU", "ODEF", "OLEF")]
        [InlineData("ADEU", "AFIF", "ADEU", "ADEF", "ADIF", "AFIF")]
        [InlineData("OLAF", "AFIF", "OLAF", "OLEF", "ODEF", "ADEF", "ADIF", "AFIF")]
        public async Task QueueFinder_GivenWords_ShouldReturnExpectedResult(string word, string word2, params string[] expectedlist)
        {
            var strategy = new QueueFinderStrategy();

            var result = await strategy.FindAsync(word, word2, listTest);

            Assert.Equal(expectedlist.Length, result.Count());
            Assert.Equal(string.Join("->", expectedlist), string.Join("->", result));
        }

        [Theory]
        [InlineData("KILO", "OLEF")]
        [InlineData("KILO", "AFIF")]
        public async Task QueueFinder_GivenWords_ShouldReturnNull(string word, string word2)
        {
            var strategy = new QueueFinderStrategy();

            var result = await strategy.FindAsync(word, word2, listTest);

            Assert.Null(result);
        }
    }
}
