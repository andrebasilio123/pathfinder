﻿using Microsoft.Extensions.Logging;
using Moq;
using PathFinder.Contract.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace PathFinder.Implementation.Test.Builders
{
    public class WordsFilteredPathBuilderTests
    {
        [Fact]
        public async Task WordsFilteredPathBuilder_GivenInvalidArgs_ShouldReturnError()
        {
            //Arrange
            var builder = new WordsFilteredPathBuilderTestsBuilder()
                            .WithReaderFilter(false);

            //Act
            await builder.Build().BuildAsync(new string[] { "test" });

            //Assert
            builder.VerifyLoggerMessage(ErrorMessage.InvalidLengthOfWordsArguments, Times.Once());

            //Act
            await builder.Build().BuildAsync(new string[] { "test", "test2", "test3", "ola" });

            //Assert
            builder.VerifyLoggerMessage(ErrorMessage.InvalidNumberOfArguments, Times.Exactly(2));

            //Act
            await builder.Build().BuildAsync(new string[] { "test", "test", "test" });

            //Assert
            builder.VerifyLoggerMessage(ErrorMessage.InvalidLengthOfWordsArguments, Times.Exactly(3));
            builder.VerifyAll();
        }

        [Fact]
        public async Task WordsFilteredPathBuilder_GivenValidArgs_ShouldReturnSuccess()
        {
            //Arrange
            var source = "test.txt";
            var word1 = "adeu";
            var word2 = "fufu";
            var destination = "destination";
            var findPath = new List<string> { "adeu", "fdeu", "fdfu", "fufu" };

            var builder = new WordsFilteredPathBuilderTestsBuilder()
                            .WithReaderFilter(true)
                            .WithReadAsync()
                            .WithFindAsync(findPath)
                            .WithWriteAsync();

            //Act
            await builder.Build().BuildAsync(new string[] { source, word1, word2, destination });

            //Assert
            builder.VerifyLoggerMessage($"Starting {source} {word1} {word2} {destination}", Times.Exactly(2), LogLevel.Information);
            builder.VerifyLoggerMessage($"End {string.Join(" -> ", findPath)}", Times.Exactly(2), LogLevel.Information);
        }
    }
}
