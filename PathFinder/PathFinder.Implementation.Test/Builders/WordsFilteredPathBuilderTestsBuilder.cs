﻿using Microsoft.Extensions.Logging;
using Moq;
using PathFinder.Contract.Finders;
using PathFinder.Contract.Readers;
using PathFinder.Contract.Writers;
using PathFinder.Implementation.Builders;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PathFinder.Implementation.Test.Builders
{
    public class WordsFilteredPathBuilderTestsBuilder
    {
        public readonly Mock<ILogger<WordsFilteredPathBuilder>> Logger = new Mock<ILogger<WordsFilteredPathBuilder>>();
        private readonly Mock<IFilterReaderFile> _reader = new Mock<IFilterReaderFile>(MockBehavior.Strict);
        private readonly Mock<IFinderStrategy> _finderStrategy = new Mock<IFinderStrategy>(MockBehavior.Strict);
        private readonly Mock<IWriterFile> _writer = new Mock<IWriterFile>(MockBehavior.Strict);

        public WordsFilteredPathBuilder Build() 
        {
            return new WordsFilteredPathBuilder(
                Logger.Object,
                _reader.Object,
                _finderStrategy.Object,
                new List<IWriterFile> { _writer.Object }
            ) ;
        }

        public WordsFilteredPathBuilderTestsBuilder WithReaderFilter(bool returnFilter) 
        {
            var filterWord = new Mock<IFilterWord>();
            filterWord.Setup(x => x.FilterAsync(It.IsAny<string>())).ReturnsAsync(returnFilter);
            _reader.Setup(x => x.GetFilter()).Returns(filterWord.Object);

            return this;
        }

        public WordsFilteredPathBuilderTestsBuilder WithReadAsync() 
        {
            _reader.Setup(x => x.ReadAsync(It.IsAny<string>())).ReturnsAsync(new List<string>());

            return this;
        }

        public WordsFilteredPathBuilderTestsBuilder WithFindAsync(List<string> findPath) 
        {
            _finderStrategy.Setup(x => x.FindAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IEnumerable<string>>()))
                .ReturnsAsync(findPath);

            return this;
        }

        public WordsFilteredPathBuilderTestsBuilder WithWriteAsync() 
        {
            _writer.Setup(x => x.WriteAsync(It.IsAny<string>(), It.IsAny<IEnumerable<string>>()))
                .Returns(Task.CompletedTask);

            return this;
        }

        public void VerifyLoggerMessage(string message, Times times, LogLevel level = LogLevel.Error)
        {
            Logger.Verify( x => x.Log(
                            level,
                            It.IsAny<EventId>(),
                            It.IsAny<It.IsAnyType>(),
                            It.IsAny<Exception>(),
                            (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()),
                            times, message
                    );
        }

        public void VerifyAll() 
        {
            _reader.VerifyAll();
            _finderStrategy.VerifyAll();
            _writer.VerifyAll();
        }
    }
}
