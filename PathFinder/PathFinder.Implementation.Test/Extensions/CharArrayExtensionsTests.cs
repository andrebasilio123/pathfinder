using PathFinder.Implementation.Extensions;
using Xunit;

namespace PathFinder.Implementation.Test.Extensions
{
    public class CharArrayExtensionsTests
    {
        [Theory]
        [InlineData("Adeu", "Odeu", 1)]
        [InlineData("", "Odeu", 4)]
        [InlineData("Odeu", "", 4)]
        [InlineData("Adeus", "Odeu", 2)]
        [InlineData("Adeu", "ODEU", 4)]
        [InlineData("Adeu", "OdEu", 2)]
        [InlineData("Adeu", "OdEuas", 4)]
        [InlineData("OdEuas", "Adeu", 4)]
        [InlineData("Adeu", "Adeu", 0)]
        [InlineData("Adeu", "adeu", 1)]
        [InlineData("Adeu", "ADEU", 3)]
        [InlineData("", "", 0)]
        public void NumberDifferentLetters_GivenCharArray_ShouldReturnExpectedResult(string a, string b, int expectedReturn)
        {
            var result = a.ToCharArray().NumberDifferentLetters(b.ToCharArray());

            Assert.Equal(expectedReturn, result);
        }
    }
}
