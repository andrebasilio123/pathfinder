﻿using PathFinder.Implementation.Readers;
using System.Threading.Tasks;
using Xunit;

namespace PathFinder.Implementation.Test.Readers
{
    public class FourLettersFilterWordTests
    {

        [Theory]
        [InlineData("1234", true)]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("123", false)]
        [InlineData("12312", false)]
        public async Task FourLettersFilter_GivenWord_ShouldReturnExpectedResult(string word, bool expectedResult) 
        {
            var filter = new FourLettersFilterWord();

            var result = await filter.FilterAsync(word);

            Assert.Equal(expectedResult, result);
        }
    }
}
