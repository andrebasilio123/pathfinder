﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PathFinder.Infrastruture.Extensions;
using PathFinder.Services;
using System.Threading.Tasks;

namespace PathFinder
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var services = new ServiceCollection()
                               .ConfigureServices()
                               .BuildServiceProvider();

            var service = services.GetService<IRunner>();

            await service.RunAsync(args);
        }
    }

    public static class ProgramExtensions 
    {
        internal static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddLogging(configure => configure.AddConsole())
                    .AddTransient<IRunner, Runner>()
                    .AddPathFinderContracts();

            return services;
        }
    }
}
