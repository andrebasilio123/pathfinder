﻿using System.Threading.Tasks;

namespace PathFinder.Services
{
    public interface IRunner
    {
        public Task RunAsync(string[] args);
    }
}
