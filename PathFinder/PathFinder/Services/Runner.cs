﻿using PathFinder.Contract.Builders;
using System.Threading.Tasks;

namespace PathFinder.Services
{
    public class Runner : IRunner
    {
        private readonly IPathBuilder _pathBuilder;

        public Runner(IPathBuilder pathBuilder)
        {
            _pathBuilder = pathBuilder;
        }

        public async Task RunAsync(string[] args)
        {
            await _pathBuilder
                    .BuildAsync(args);
        }
    }
}
