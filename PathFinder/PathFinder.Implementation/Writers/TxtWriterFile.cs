﻿using PathFinder.Contract.Writers;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PathFinder.Implementation.Writers
{
    public class TxtWriterFile : IWriterFile
    {
        public async Task WriteAsync(string filePath, IEnumerable<string> lines)
        {
            if (lines != null)
                File.WriteAllLines($"{filePath}.txt", lines);

            await Task.CompletedTask;
        }
    }
}
