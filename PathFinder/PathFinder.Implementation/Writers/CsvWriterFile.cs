﻿using PathFinder.Contract.Writers;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PathFinder.Implementation.Writers
{
    public class CsvWriterFile : IWriterFile
    {
        public async Task WriteAsync(string filePath, IEnumerable<string> lines)
        {
            if (lines != null)
                File.WriteAllText($"{filePath}.csv", string.Join(";", lines));

            await Task.CompletedTask;
        }
    }
}
