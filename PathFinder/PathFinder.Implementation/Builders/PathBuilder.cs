﻿using Microsoft.Extensions.Logging;
using PathFinder.Contract.Builders;
using PathFinder.Contract.Exceptions;
using PathFinder.Contract.Finders;
using PathFinder.Contract.Readers;
using PathFinder.Contract.Writers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PathFinder.Implementation.Builders
{
    public class PathBuilder : IPathBuilder
    {
        protected readonly ILogger<PathBuilder> _logger;
        private readonly IReaderFile _reader;
        private readonly IFinderStrategy _finderStrategy;
        private readonly IEnumerable<IWriterFile> _writers;

        public PathBuilder(ILogger<PathBuilder> logger, IReaderFile reader, IFinderStrategy finderStrategy, IEnumerable<IWriterFile> writers)
        {
            _logger = logger;
            _reader = reader;
            _finderStrategy = finderStrategy;
            _writers = writers;
        }

        private string SourcePath { get; set; }
        private string StartWord { get; set; }
        private string EndWord { get; set; }
        private string DestinationPath { get; set; }

        protected virtual async Task<bool> AddAndValidateArgs(string[] args)
        {
            await Task.CompletedTask;
            
            if (args.Length != 4)
            {
                _logger.LogError(ErrorMessage.InvalidNumberOfArguments);
                return false;
            }

            StartWord = args[0];
            EndWord = args[1];
            SourcePath = args[2];
            DestinationPath = args[3];

            return true;
        }

        public async Task BuildAsync(string[] args)
        {
            IEnumerable<string> findPath = null;
            if (await AddAndValidateArgs(args)) {

                _logger.LogInformation($"Starting {StartWord} {EndWord} {SourcePath} {DestinationPath}");

                var lines = await _reader.ReadAsync(SourcePath);

                findPath = await _finderStrategy.FindAsync(StartWord, EndWord, lines);

                foreach (var writer in _writers)
                {
                    await writer.WriteAsync(DestinationPath, findPath);
                }
            }

            _logger.LogInformation($"End {string.Join(" -> ", findPath ?? new List<string> { $"no path found between {StartWord} {EndWord}" })}");
        }
    }
}
