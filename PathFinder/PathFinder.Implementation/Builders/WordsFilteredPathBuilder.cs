﻿using Microsoft.Extensions.Logging;
using PathFinder.Contract.Exceptions;
using PathFinder.Contract.Finders;
using PathFinder.Contract.Readers;
using PathFinder.Contract.Writers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PathFinder.Implementation.Builders
{
    public class WordsFilteredPathBuilder : PathBuilder
    {
        private readonly IFilterWord _filterWord;

        public WordsFilteredPathBuilder(ILogger<WordsFilteredPathBuilder> logger, IFilterReaderFile reader, IFinderStrategy finderStrategy, IEnumerable<IWriterFile> writers)
        :base(logger, reader, finderStrategy, writers)
        {
            _filterWord = reader.GetFilter();
        }

        protected override async Task<bool> AddAndValidateArgs(string[] args) 
        {
            var result = await base.AddAndValidateArgs(args);

            if (result && (!await _filterWord.FilterAsync(args[0]) || !await _filterWord.FilterAsync(args[1])))
            {
                _logger.LogError(ErrorMessage.InvalidLengthOfWordsArguments);
                return false;
            }

            return result;
        }
    }
}
