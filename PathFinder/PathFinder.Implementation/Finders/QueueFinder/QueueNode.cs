﻿using System.Collections.Generic;

namespace PathFinder.Implementation.Finders.QueueFinder
{
    public class QueueNode
    {
        public List<string> Path;
        public string CurrentWord;

        public QueueNode(string word)
        {
            Path = new List<string>();
            AddWord(word);
        }

        public QueueNode(string word, List<string> path)
        {
            Path = new List<string>(path);
            AddWord(word);
        }

        private void AddWord(string word)
        {
            Path.Add(word);
            CurrentWord = word;
        }

        public List<string> GetPath()
        {
            return Path;
        }
    }
}
