﻿using PathFinder.Contract.Finders;
using PathFinder.Implementation.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder.Implementation.Finders.QueueFinder
{
    public class QueueFinderStrategy : IFinderStrategy
    {
        public Queue<QueueNode> queue;

        private async Task InicializeQueue(string startWord)
        {
            queue = new Queue<QueueNode>();
            queue.Enqueue(new QueueNode(startWord));

            await Task.CompletedTask;
        }

        public async Task<IEnumerable<string>> FindAsync(string startWord, string endWord, IEnumerable<string> lines)
        {
            await InicializeQueue(startWord);


            while (queue.Count > 0)
            {
                var pathNode = queue.Dequeue();
                var nodesToRemove = new List<string>();
                foreach (var line in lines)
                {
                    if (line.ToCharArray().NumberDifferentLetters(pathNode.CurrentWord.ToCharArray()) == 1)
                    {
                        var newPathNode = new QueueNode(line, pathNode.Path);
                        queue.Enqueue(newPathNode);
                        nodesToRemove.Add(line);

                        if (line.Equals(endWord))
                            return newPathNode.GetPath();
                    }
                }
                lines = lines.Except(nodesToRemove).ToList();
            }

            return null;
        }
    }
}
