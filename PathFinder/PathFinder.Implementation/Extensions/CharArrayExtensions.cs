﻿using System;

namespace PathFinder.Implementation.Extensions
{
    public static class CharArrayExtensions
    {
        public static int NumberDifferentLetters(this char[] a1, char[] a2)
        {
            int count = 0;

            for (int i = 0; i < Math.Min(a1.Length, a2.Length); i++)
                if (a1[i] != a2[i])
                    count++;

            count += Math.Abs(a1.Length - a2.Length);

            return count;
        }
    }
}
