﻿using PathFinder.Contract.Readers;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PathFinder.Implementation.Readers
{
    public class FilterReaderFile : IFilterReaderFile
    {
        private readonly IFilterWord _filterWord;

        public FilterReaderFile(IFilterWord filterWord)
        {
            _filterWord = filterWord;
        }

        public IFilterWord GetFilter()
        {
            return _filterWord;
        }

        public async Task<IEnumerable<string>> ReadAsync(string filePath)
        {
            var listToReturn = new List<string>();

            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = await reader.ReadLineAsync()) != null)
                {
                    if (await _filterWord.FilterAsync(line))
                        listToReturn.Add(line);
                }
            }

            return listToReturn;
        }
    }
}
