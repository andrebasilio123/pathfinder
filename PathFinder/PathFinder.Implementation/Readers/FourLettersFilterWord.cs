﻿using PathFinder.Contract.Readers;
using System.Threading.Tasks;

namespace PathFinder.Implementation.Readers
{
    public class FourLettersFilterWord : IFilterWord
    {
        public int NumberOfLetters { get => 4; }

        public async Task<bool> FilterAsync(string line) => await Task.FromResult(line?.Length == NumberOfLetters);
    }
}
