# PathFinder

    This console application input parameters are two words, a source file that contains a set of words and 
    destination file to store the final result. The application should calculate the shortest list of words, 
    the words should be present on source file, where each word differs from the previous word by one letter only. 
    The result should be written on the destination.

    Example args: Adeu Olef fileWords finalFile (fileWords file contais words Adeu Odeu Edeu Olaf Olef Odef)
            finalFile result file should have the words Adeu Odeu Odef Olef


## Project Solution

    On the solution you will find the following projects:
        - PathFinder
        - PathFinder.Contract
        - PathFinder.Contract.Implementation
        - PathFinder.Contract.Implementation.Test
        - PathFinder.Infrastruture

### PathFinder

    The project represents the console app, its responsability is to configure the services, receive the entry 
    values and know wich contract should be called with these values, it gives the flexibility to change the way that 
    data is received, meaning, the PathFinder project can be replaced by any other project without any change on the 
    other projects.  
     
### PathFinder.Contract

    The project represents the contracts (interfaces) of the solution.

### PathFinder.Contract.Implementation

    The project represents the contracts implementation, that give a flexibility to have more projects with different 
    implementations of the same contracts.

### PathFinder.Contract.Implementation.Test

    The project represents the unit test of the implementation. It was used Moq and Xunit to make the tests.

### PathFinder.Infrastruture

    The project represents the configuration of which implementation should be used by PathFinder. It gives a 
    flexibility to change the implementation of all solution.  


## Some parts to emphasize on architecture

    Builders - Responsible to know how to run the solution, have one simple approach "PathBuilder" and another one 
    "WordsFilteredPathBuilder" to be possible to use filtering on entry values, supporting on decorator pattern. 
    Builder is composed by Reader (strategy pattern), FinderStrategy (strategy pattern) and 
    list of writers (strategy pattern).

    
    Readers - Responsible to read. The readers implementation has filters, to be possible to filter by word, this 
    allows to change the strategy of the filter by word without changing the reader. (Example: create one strategy 
    filtering by 5 letters instead of 4). Here you'll find a IReaderFile that allows to build reader without filter. 


    Finders - Responsible to find the path. QueueFinderStrategy has an implementation by queue with LIFO 
    (last in frist out) approach to make possible to find the best path between the two words. Exist one list with 
    the words that already not used, it prevents to pass in same word more than 1 time.


    Writers - Responsible to write, here the approach was create a list of strategies to be possible to write on one 
    or more implementations at the same time. This allows to create and insert an implementation without changing 
    anything. On the code you can see two implementations, csv and txt writers to illustrate that. 


    Tests - On this project exists Builders responsible to help the test build the entity. This approach makes it 
    possible to test the important part and mock the rest of the code. 
    Builder example: WordsFilteredPathBuilderTestsBuilder. 

